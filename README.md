Welcome!

In order to setup backend you need:

 - **1. Clone this repo**
```
git clone https://gitlab.com/ladacumakova5/taro.git
```
 - **2.Inside cloned repo create python virtual envirement**
  For example:
```
python -m venv venv
```
- **3.After you activate venv, install all of the dependencies**

Windows:  
```  
venv/scripts/activate  
```  
 Linux:
```
source venv/bin/activate 
```
- **4. go to into prediction directory and run server**
```
cd taro
cd prediction
docker-compose up --build
```