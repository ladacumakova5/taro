from django.urls import path
 
from taro.views import *

urlpatterns = [
    path('', firstAPIView.as_view()),
    path('future/', futureAPIView.as_view(), name = 'future'),
    path('card_of_the_day/', card_of_the_dayAPIView.as_view(), name = 'card_of_the_day'),
    path('situation/', situationAPIView.as_view(), name = 'situation'),
    path('yes_or_not/', yes_or_notAPIView.as_view(), name = 'yes_or_not'),
    path('card_meanings/', cards_meaningAPIView.as_view(), name = 'card_meanings'),
    path('appointment/', appointmentAPIView.as_view(), name = 'appointment'),
    path('appointment/<int:pk>/', appointmentAPIDetailView.as_view()),
]