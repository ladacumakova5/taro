from django.http import HttpResponse, HttpResponseNotFound, HttpRequest
from .models import Card, Appointment
from rest_framework import permissions, status, generics
from rest_framework.response import Response
import random
from datetime import datetime, timedelta, date

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, UpdateAPIView
from rest_framework.permissions import IsAdminUser

from django.template.loader import render_to_string
from .serializers import Serializer, AppointmentSerializer, AppSerializer


# Create your views here.
class firstAPIView(APIView):
    def get(self, request):
        queryset = Card.objects.all()
        return Response({'posts': Serializer(queryset, many = True).data}, status=status.HTTP_200_OK)


class futureAPIView(APIView):
    serializer_class = Serializer

    def get(self, request):
        queryset = Card.objects.order_by('?')[:1]
        return Response({'first': Serializer(queryset, many = True).data}, status=status.HTTP_200_OK)

class card_of_the_dayAPIView(APIView):
    serializer_class = Serializer

    def get(self, request):
        queryset = Card.objects.order_by('?')[:1]
        return Response({'first': Serializer(queryset, many = True).data}, status=status.HTTP_200_OK)

class situationAPIView(APIView):    #выводится 3 карты
    serializer_class = Serializer

    def get(self, request):
        queryset = Card.objects.order_by('?')
    
        first_card = queryset.first()
        second_card = queryset.exclude(id=first_card.id).first()
        third_card = queryset.exclude(id__in=[first_card.id, second_card.id]).first()
        
        first_data = Serializer([first_card], many=True).data
        second_data = Serializer([second_card], many=True).data
        third_data = Serializer([third_card], many=True).data
        
        return Response({'first': first_data, 'second': second_data, 'third': third_data}, status=status.HTTP_200_OK)
        

class yes_or_notAPIView(APIView):  
    serializer_class = Serializer

    def get(self, request):
        queryset = Card.objects.order_by('?')[:1]
        return Response({'first': Serializer(queryset, many = True).data}, status=status.HTTP_200_OK)

class cards_meaningAPIView(APIView):  
    serializer_class = Serializer

    def get(self, request):
        queryset = Card.objects.order_by('?')[:1]
        return Response({'first': Serializer(queryset, many = True).data}, status=status.HTTP_200_OK)


class appointmentAPIView(APIView):
    serializer_class = AppointmentSerializer
    queryset = Appointment.objects.all()

    def get(self, request, *args, **kwargs):
        all_time = ['10:00', '11:00', '12:00', '13:00', '14:00',
                    '15:00', '16:00', '17:00', '18:00']

        day = datetime.today()
        min_day_value = day.strftime("%Y-%m-%d")
    
        recording_date = self.request.query_params.get('recording_date')
        if recording_date is None:
            app = {
                'min_day_value': min_day_value,
                'all_time': all_time,
                'step_1': True,
                'step': 'Шаг 1'
            }
            return Response(app, status=status.HTTP_200_OK)
        else:
            appoint = Appointment.objects.filter(recording_date=recording_date).all()
            for obj in appoint:
                all_time.remove(obj.recording_date.strftime("%H:%M"))
            app2 = {
                'min_day_value': min_day_value,
                'all_time': all_time,
                'step_1': False,
                'step_2': True,
                'step': 'Шаг 2',
                'choised_day': recording_date
            }
            return Response(app2, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = AppSerializer(data = request.data)
        serializer.is_valid(raise_exception = True)
        serializer.save()
        return Response(status=status.HTTP_200_OK)


class appointmentAPIDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AppointmentSerializer
    queryset = Appointment.objects.all()
    permission_classes = (IsAdminUser, )

def page_not_found(request, exception):
    return HttpResponseNotFound("Страница не найдена")