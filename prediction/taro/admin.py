from django.contrib import admin
from .models import Card, Appointment


# class PhotoInline(admin.StackedInline):
#     model = CardPhoto


@admin.register(Card)
class MapAdmin(admin.ModelAdmin):
    list_display = "name", "interpretation_sh", "short_interpretation", "type", "photo"
    # inlines = [PhotoInline]
    search_fields = "name",

    #def interpretation_sh(self, obj: Card) -&gt; str:
    #   if len(self.interpretation) < 30:
    #        return self.interpretation
    #    return self.interpretation[:30] + "..."


@admin.register(Appointment)
class MapAdmin(admin.ModelAdmin):
    list_display = "user_id", "name", "email", "recording_date", "time"
    # inlines = [PhotoInline]
    search_fields = "name", "email",