from django.db import models
from datetime import datetime



class Card(models.Model):
    name = models.CharField(max_length=90)
    interpretation = models.CharField(max_length=1200)
    short_interpretation = models.CharField(max_length=90)
    type = models.CharField(max_length=90)
    photo = models.CharField()

    @property
    def interpretation_sh(self) -> str:
        if len(self.interpretation) < 50:
            return self.interpretation
        return self.interpretation[:50] + "..."


    def __str__(self):
        return self.name


class Appointment(models.Model):
    user_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    recording_date = models.DateField()
    time = models.TimeField()
    

    def __str__(self):
        return self.name