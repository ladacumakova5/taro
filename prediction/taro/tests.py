from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from datetime import date

from .models import Card, Appointment

class CardModelTest(TestCase):
    def setUp(self):
        self.card = Card.objects.create(
            name='Test Card',
            interpretation='Test Interpretation',
            short_interpretation='Test Short Interpretation',
            type='Test Type',
            photo='test.jpg'
        )

    def test_card_str(self):
        self.assertEqual(str(self.card), 'Test Card')

class AppointmentModelTest(TestCase):
    def setUp(self):
        self.appointment = Appointment.objects.create(
            name='Test User',
            email='test@example.com',
            recording_date=date.today(),
            time='10:00'
        )

    def test_appointment_str(self):
        self.assertEqual(str(self.appointment), 'Test User')

class futureAPIViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.future_url = reverse('future')
    
    def test_get_available_data(self):
        response = self.client.get(self.future_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertIsInstance(data['first'], list)
        self.assertGreater(len(data['first']), 0)
        
        card = data['first'][0]
        self.assertIsInstance(card, dict)
        
        self.assertIn('name', card)
        self.assertIsInstance(card['name'], str)
        
        self.assertIn('interpretation', card)
        self.assertIsInstance(card['interpretation'], str)
        
        self.assertIn('short_interpretation', card)
        self.assertIsInstance(card['short_interpretation'], str)
        
        self.assertIn('type', card)
        self.assertIsInstance(card['type'], str)
        
        self.assertIn('photo', card)
        self.assertIsInstance(card['photo'], str)

class AppointmentAPIViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.appointment_url = reverse('appointment')

    def test_get_available_appointments(self):
        response = self.client.get(self.appointment_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('min_day_value', response.data)
        self.assertIn('all_time', response.data)
        self.assertIn('step_1', response.data)
        self.assertIn('step', response.data)

    def test_get_available_appointments_with_recording_date(self):
        recording_date = date.today()
        response = self.client.get(self.appointment_url, {'recording_date': recording_date})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('min_day_value', response.data)
        self.assertIn('all_time', response.data)
        self.assertIn('step_1', response.data)
        self.assertIn('step_2', response.data)
        self.assertIn('step', response.data)
        self.assertEqual(response.data['choised_day'], str(recording_date))

    def test_create_appointment(self):
        data = {
            'name': 'Test User',
            'email': 'test@example.com',
            'recording_date': str(date.today()),
            'time': '10:00'
        }
        response = self.client.post(self.appointment_url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Appointment.objects.count(), 1)
