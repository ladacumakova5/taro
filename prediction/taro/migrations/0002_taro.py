from django.db import migrations

def create_data(apps, schema_editor):
    Card = apps.get_model('taro', 'Card')

    Card(name='star_card', interpretation='Указывает на происходящие изменения к лучшему. Аркан символизирует восход солнца, а это является предвестником новых идей и стремлений. Карта звезда дает надежду на хорошее будущее', 
    short_interpretation='перспективы', type='17 старший аркан', photo='https://i.pinimg.com/736x/33/95/6d/33956d1f583f3f1116c42c313011ffcc.jpg').save()

    Card(name='strength_card', interpretation='Карта говорит о стойкости характера, особенно в моменты опасности или в период тяжелых ситуаций. О возможности быть в выигрышном положении благодаря терпимости и внутреннему спокойстви',
    short_interpretation='самодисциплина', type='8 старший аркан', photo='https://i.pinimg.com/736x/e4/fb/df/e4fbdf6ec4c4d34a3ea4ca8d4561a30d.jpg').save()

    Card(name='lovers_card', interpretation='Карта указывает на то, что человеку предстоит принять решение. Прошлое и будущее, старое и новое, обязанности и желания - это то, между чем очень скоро предстоит сделать выбор человеку',
    short_interpretation='важный выбор', type='6 старший аркан', photo='https://i.pinimg.com/474x/e6/a0/ea/e6a0eabcfc68fc01ab4cdcda9560ed05.jpg').save()

    Card(name='fortune_card', interpretation='Карта указывает на наступление благоприятного периода в жизни. Это могут быть приятные события, радостные новости, успех в делах или открытие новых возможностей',
    short_interpretation='прогресс', type='10 старший аркан', photo='https://i.pinimg.com/474x/27/30/7b/27307ba170d3dc80b3469d74dde91c28.jpg').save()

    Card(name='devil_card', interpretation='Карта указывает на зависимость от кого-то или чего-то, слабость силы воли, несдержанность, пренебрежение собственными моральными принципами',
    short_interpretation='зависимость', type='15 старший аркан', photo='https://i.pinimg.com/474x/e2/00/15/e20015fce63898280d0a1188db35f1c5.jpg').save()
    

    


class Migration(migrations.Migration):

    dependencies = [
        ('taro', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_data),
    ]