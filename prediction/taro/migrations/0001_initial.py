from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        
    ]

    operations = [
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=90)),
                ('interpretation', models.CharField(max_length=1200)),
                ('short_interpretation', models.CharField(max_length=90)),
                ('type', models.CharField(max_length=90)),
                ('photo', models.CharField()),
            ],
        ),
    ]