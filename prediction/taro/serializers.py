from rest_framework import serializers
from .models import Card, Appointment
from django.utils import timezone


class Serializer(serializers.ModelSerializer):

    class Meta:
        model = Card
        fields = '__all__'

class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = ['name', 'email', 'recording_date', 'time']

    def validate_recording_date(self, value):
        # Check if the recording date is within the next week
        if value < timezone.now().date() or value > timezone.now().date() + timezone.timedelta(days=7):
            raise serializers.ValidationError("Записаться можно только на неделю вперед!")
        return value

    def validate_time(self, value):
        # Check if the appointment time is within the allowed range
        allowed_times = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        if value.hour not in allowed_times:
            raise serializers.ValidationError("Записаться можно только с 8 до 20!")
        return value

class AppSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = ['name', 'email', 'recording_date', 'time']
    name = serializers.CharField(max_length=50)
    email = serializers.EmailField(max_length=50)
    recording_date = serializers.DateField()
    time = serializers.TimeField()

    def create(self, validate_data):
        return Appointment.objects.create(**validate_data)