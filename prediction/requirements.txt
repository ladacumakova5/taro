Django==4.2.7
psycopg2==2.9.9
psycopg2-binary==2.9.5
djangorestframework
django-cors-headers
Pillow==9.0.1